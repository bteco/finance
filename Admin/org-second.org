ORG Second Mode -*- mode: org; -*-

* COMMENT HEAD
#+TITLE:     Tool to Get Organised
#+AUTHOR:    Gunter Liszewski
#+EMAIL:     gunter.liszewski@gmail.com
#+DATE:      2012-03-28 Wed
#+LANGUAGE:  en
#+OPTIONS:   H:3 num:t toc:t \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:nil skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:http://orgmode.org/org-info.js
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+LINK_UP:   
#+LINK_HOME: 

* COMMENT TODO Write-up of the Plan to Use the (not so) personal Organiser, emacs org mode

* Org-Mode - The Personal Organiser for G...ee...x

** Being Portable

+ [[https://github.com/richard/mobileorg][MobileOrg]], an iPhone App to work with Emacs Org-Mode outline in the mobile
world. The mobileorg source code is hosted presented as a Git repository on
[[http:/github.com][Github]].

** Moving along the Lines of The Outline [fn:moving: On a Foot-Note -- staggering about ]
** What is Organisation

*** Outlines

*** Plans

*** Things To Do

*** Time

** The Outline Structure

*** Keys to Press
+ C-c C-e ('org-export')
+ C-c TAB ('show-children')
+ C-c C-s (schedule)
*** Commands to Give

*** Planning to Organise my Moves

** What Plans are to Be Done

*** To-Do Items

*** Communicating with the Organiser; To-Do items' states

* Org-Mode - The Manual 

  + [[info: (Org)]]
  + [[http://mobileorg.ncogni.to/][MobileOrg]]
  + [[http://orgmode.org/][Org Mode]]
    - [[info:emacs:Top][Info (emacs) Top]]
